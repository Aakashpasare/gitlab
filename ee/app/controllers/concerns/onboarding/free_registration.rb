# frozen_string_literal: true

module Onboarding
  class FreeRegistration
    def self.redirect_to_company_form?
      false
    end
  end
end
